﻿using DotnetApp.Models;
using System.ComponentModel.DataAnnotations;

namespace DotnetApp.ViewModel
{
    public class StudentViewModel
    {
        [Display(Name = "Student Id")]
        public virtual int student_id { get; set; }
        [Display(Name = "Student Name")]
        public virtual string student_name { get; set; }
        [Display(Name = "Student Address")]
        public virtual string student_address { get; set; }
        [Display(Name = "Student Contact")]
        public virtual string student_contact { get; set; }
        [Display(Name = "Grade")]
        public virtual int grade_id { get; set; }
        public virtual int grade_name { get; set; }
    }
}