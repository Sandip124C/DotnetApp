﻿using System;
using ClientNotifications;
using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using static ClientNotifications.Helpers.NotificationHelper;

namespace DotnetApp.Controllers
{
    public class GradeController : Controller
    {
        private IGradeRepository _gradeRepository;
        private IGradeService _gradeService;
        private IClientNotification _clientNotification;

        public GradeController(IGradeRepository gradeRepository,
                                IGradeService gradeService,
                                IClientNotification clientNotification)
        {
            _gradeRepository = gradeRepository;
            _gradeService = gradeService;
            _clientNotification = clientNotification;

        }

        public ActionResult Index()
        {
            var grades = _gradeRepository.GetAllGrades();
            return View(grades);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            try
            {
                var single_grade = _gradeService.GetById(id);
                return View(single_grade);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
                return RedirectToAction(nameof(Index));
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Grade grade)
        {
            try
            {
                _gradeService.Create(grade);
                _clientNotification.AddSweetNotification("Success", "Grade Successfully Created.", NotificationType.success);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
                return View();
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var single_grade = _gradeService.GetById(id);
                return View(single_grade);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        public ActionResult Edit(Grade grade)
        {
            try
            {
                _gradeService.Update(grade);
                _clientNotification.AddSweetNotification("Success", "Grade Successfully Updated.", NotificationType.success);

            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
                return View();
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public ActionResult Delete(Grade grade)
        {
            try
            {
                _gradeService.Delete(grade);
                _clientNotification.AddSweetNotification("Success", "Grade Successfully Deleted.", NotificationType.success);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
            }
            return RedirectToAction(nameof(Index));
        }
    }
}