﻿using System;
using System.Collections.Generic;
using ClientNotifications;
using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Services.Interface;
using DotnetApp.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using static ClientNotifications.Helpers.NotificationHelper;

namespace DotnetApp.Controllers
{
    public class StudentController : Controller
    {
        private IStudentRepository _studentRepository;
        private IGradeRepository _gradeRepository;
        private IStudentService _studentService;
        private IClientNotification _clientNotification;

        public StudentController(IStudentRepository studentRepository,
                                IGradeRepository gradeRepository,
                                IStudentService studentService,
                                 IClientNotification clientNotification)
        {
            _studentRepository = studentRepository;
            _gradeRepository = gradeRepository;
            _studentService = studentService;
            _clientNotification = clientNotification;
        }

        public IActionResult Index()
        {
            var students = _studentRepository.GetAllStudents();
            return View(students);
        }

        public IActionResult Create()
        {
            var grades = new List<Grade>();
            grades.AddRange(_gradeRepository.GetAllGrades());
            ViewBag.grades = new SelectList(grades, "grade_id", "grade_name");
            return View();
        }

        [HttpPost]
        public IActionResult Create(Student student)

        {
            try
            {
                _studentService.Create(student);
                _clientNotification.AddSweetNotification("Success","Student Successfully Created.", NotificationType.success);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error",e.Message.ToString(), NotificationType.error);
                return View();
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            try
            {
                var single_student = _studentService.GetById(id);
                return View(single_student);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            try
            {
                var single_student = _studentService.GetById(id);
                return View(single_student);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
                return RedirectToAction(nameof(Index));
            }
        }

        [HttpPost]
        public IActionResult Edit(Student student)
        {
            try
            {
                _studentService.Update(student);
                _clientNotification.AddSweetNotification("Success", "Student Successfully Updated.", NotificationType.success);

            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
                return View();
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Delete(Student student)
        {
            try
            {
                _studentService.Delete(student);
                _clientNotification.AddSweetNotification("Success", "Student Successfully Deleted.", NotificationType.success);
            }
            catch (Exception e)
            {
                _clientNotification.AddSweetNotification("Error", e.Message.ToString(), NotificationType.error);
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
