﻿using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Session_Factory;
using NHibernate;
using System.Collections.Generic;

namespace DotnetApp.Repository
{
    public class StudentRepository: IStudentRepository
    {
        public void Create(Student student)
        {
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Save(student);
                    tx.Commit();
                }
            }
        }

        public void Delete(Student student)
        {
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Delete(student);
                    tx.Commit();
                }
            }
        }

        public void Edit(Student student)
        {
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Update(student);
                    tx.Commit();
                }
            }
        }

        public Student GetSingleStudentById(int id)
        {
            Student student;
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                student = session.QueryOver<Student>().Where(a => a.student_id == id).SingleOrDefault();
            }
            return student;
        }

        public Student GetSingleStudentByName(string Name)
        {
            Student student;
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                student = session.QueryOver<Student>().Where(a => a.student_name == Name).SingleOrDefault();
            }
            return student;
        }

        IList<Student> IStudentRepository.GetAllStudents()
        {
            IList<Student> lists;

            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                lists = session.CreateCriteria<Student>().List<Student>();
            }
            return lists;
        }
    }
}
