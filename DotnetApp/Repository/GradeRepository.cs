﻿using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Session_Factory;
using NHibernate;
using System.Collections.Generic;

namespace DotnetApp.Repository
{
    public class GradeRepository : IGradeRepository
    {
        public void Create(Grade grade)
        {
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Save(grade);
                    tx.Commit();
                }
            }
        }

        public void Delete(Grade grade)
        {
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Delete(grade);
                    tx.Commit();
                }
            }
        }

        public void Edit(Grade grade)
        {
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                using (ITransaction tx = session.BeginTransaction())
                {
                    session.Update(grade);
                    tx.Commit();
                }
            }
        }

        public IList<Grade> GetAllGrades()
        {
            IList<Grade> lists;

            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                lists = session.CreateCriteria<Grade>().List<Grade>();
            }
            return lists;
        }

        public Grade GetSingleGradeById(int id)
        {
            Grade grade;
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                grade = session.QueryOver<Grade>().Where(a => a.grade_id == id).SingleOrDefault();
            }
            return grade;
        }

        public Grade GetSingleGradeByName(string Name)
        {
            Grade grade;
            using (ISession session = SessionFactory.GetSessionFactory().OpenSession())
            {
                grade = session.QueryOver<Grade>().Where(a => a.grade_name == Name).SingleOrDefault();
            }
            return grade;
        }
    }
}
