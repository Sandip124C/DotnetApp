﻿using DotnetApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetApp.Repository.Interface
{
    public interface IGradeRepository
    {
        void Create(Grade grade);
        void Edit(Grade grade);
        Grade GetSingleGradeById(int id);
        Grade GetSingleGradeByName(string Name);
        void Delete(Grade grade);
        IList<Grade> GetAllGrades();
    }
}
