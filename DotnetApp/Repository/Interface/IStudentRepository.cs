﻿using DotnetApp.Models;
using System.Collections.Generic;

namespace DotnetApp.Repository.Interface
{
    public interface IStudentRepository
    {
        void Create(Student student);
        void Edit(Student student);
        Student GetSingleStudentById(int id);
        Student GetSingleStudentByName(string Name);
        void Delete(Student student);
        IList<Student> GetAllStudents();
    }
}
