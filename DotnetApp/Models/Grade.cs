﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DotnetApp.Models
{
    public class Grade
    {
        [Display(Name = "Grade")]
        public virtual int grade_id { get; set; }
        [Display(Name = "Grade Name")]
        public virtual string grade_name{ get; set; }
        public virtual ICollection<Student> Students { get; set; }


    }
}
