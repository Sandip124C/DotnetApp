﻿using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Services.Interface;
using System;

namespace DotnetApp.Services
{
    public class GradeService : IGradeService
    {
        private IGradeRepository _gradeRepository;

        public GradeService(IGradeRepository gradeRepository)
        {
            _gradeRepository = gradeRepository;
        }
        public void Create(Grade grade)
        {
            if (_gradeRepository.GetSingleGradeByName(grade.grade_name) != null)
            {
                throw new Exception("Grade with this name already exists.");
            }
            _gradeRepository.Create(grade);
        }

        public void Delete(Grade grade)
        {
            if (grade.grade_id == 0)
            {
                throw new Exception("Invalid Grade Id.");
            }
            Grade grade1 = GetById(grade.grade_id);
            if (grade1 == null)
            {
                throw new Exception("Grade with the given id doesn't exists.");
            }
            _gradeRepository.Delete(grade1);
        }

        public Grade GetById(int id)
        {
            if (_gradeRepository.GetSingleGradeById(id) == null)
                throw new Exception("Grade with this id doesnot exists.");
            return _gradeRepository.GetSingleGradeById(id);
        }

        public Grade GetByName(string name)
        {
            if (_gradeRepository.GetSingleGradeByName(name) != null)
                throw new Exception("Grade with this name doesnot exists.");
            return _gradeRepository.GetSingleGradeByName(name);
        }

        public void Update(Grade grade)
        {
            Grade gradeByName = _gradeRepository.GetSingleGradeByName(grade.grade_name);
            if (gradeByName != null && gradeByName.grade_id != grade.grade_id)
            {
                throw new Exception("Grade With same name already exists.");
            }
            _gradeRepository.Edit(grade);
        }
    }
}
