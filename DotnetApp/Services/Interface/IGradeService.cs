﻿using DotnetApp.Models;

namespace DotnetApp.Services.Interface
{
    public interface IGradeService
    {
        void Create(Grade grade);
        void Update(Grade grade);
        void Delete(Grade grade);
        Grade GetById(int id);
        Grade GetByName(string name);
    }
}
