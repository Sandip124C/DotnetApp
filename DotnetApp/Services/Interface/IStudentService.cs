﻿using DotnetApp.Models;

namespace DotnetApp.Services.Interface
{
    public interface IStudentService
    {
        void Create(Student student);
        void Update(Student student);
        void Delete(Student student);
        Student GetById(int id);
        Student GetByName(string name);
    }
}
