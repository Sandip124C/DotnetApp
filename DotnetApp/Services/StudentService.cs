﻿using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Services.Interface;
using System;

namespace DotnetApp.Services
{
    public class StudentService : IStudentService
    {
        private IStudentRepository _studentRepository;

        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public void Create(Student student)
        {
            if (_studentRepository.GetSingleStudentByName(student.student_name) != null)
            {
                throw new Exception("Student with this name already exists.");
            }
            _studentRepository.Create(student);
        }

        public void Delete(Student student)
        {
            if (student.student_id == 0)
            {
                throw new Exception("Invalid Student Id.");
            }
            Student student1 = GetById(student.student_id);
            if (student1 == null)
            {
                throw new Exception("Student with the given id doesn't exists.");
            }
            _studentRepository.Delete(student1);
        }

        public Student GetById(int id)
        {
            if (_studentRepository.GetSingleStudentById(id) == null)
                throw new Exception("Student with this id doesnot exists.");
            return _studentRepository.GetSingleStudentById(id);
        }

        public Student GetByName(string name)
        {
            if (_studentRepository.GetSingleStudentByName(name) == null)
                throw new Exception("Student with this name doesnot exists.");
            return _studentRepository.GetSingleStudentByName(name);
        }

        public void Update(Student student)
        {
            Student studentByName = _studentRepository.GetSingleStudentByName(student.student_name);
            if (studentByName != null && studentByName.student_id != student.student_id)
            {
                throw new Exception("UserName With same name already exists.");
            }
            _studentRepository.Edit(student);
        }
    }
}
