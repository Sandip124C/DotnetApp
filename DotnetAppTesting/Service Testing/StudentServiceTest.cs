﻿using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace DotnetAppTesting.Service_Testing
{
    [TestClass]
    public class StudentServiceTest
    {
        Mock<IStudentRepository> IStudentRepository = new Mock<IStudentRepository>();

        Student student;
        StudentService studentService;

        [TestInitialize]
        public void Setup()
        {
            studentService = new StudentService(IStudentRepository.Object);
            student = new Student()
            {
                student_id = 1,
                student_name = "Sandip",
                student_address = "Arjundhara",
                student_contact = "9876543210"
            };
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Create_WhenCalled_ThrowStudentNameAlreadyExistsExceptionWhenStudentAlreadyExists()
        {
            IStudentRepository.Setup(a => a.GetSingleStudentByName(It.IsAny<string>())).Returns(new Student());
            studentService.Create(student);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Update_WhenCalled_ThrowStudentNameAlreadyExistsExceptionWhenStudentAlreadyExists()
        {
            IStudentRepository.Setup(a => a.GetSingleStudentByName(It.IsAny<string>())).Returns(new Student());
            studentService.Update(student);
        }

        [TestMethod]
        public void Create_WhenCalled_TestThatDataWillInsertWhenStudentNameIsNull()
        {
            IStudentRepository.Setup(a => a.GetSingleStudentByName(null));
            studentService.Create(student);
        }

        [TestMethod]
        public void Update_WhenCalled_TestThatDataWillUpdateWhenStudentNameIsNull()
        {
            IStudentRepository.Setup(a => a.GetSingleStudentByName(null));
            studentService.Update(student);
        }

        [TestMethod]
        public void Create_WhenCalled_TestThatDataWillSavedIntoDatabaseOrNot()
        {
            IStudentRepository.Setup(a => a.Create(It.IsAny<Student>())).Verifiable();
            studentService.Create(student);
            IStudentRepository.Verify(a => a.Create(It.IsAny<Student>()), Times.Exactly(1));

        }

        [TestMethod]
        public void Update_WhenCalled_TestThatDataWillUpdateInDatabaseOrNot()
        {
            IStudentRepository.Setup(a => a.Edit(It.IsAny<Student>())).Verifiable();
            studentService.Update(student);
            IStudentRepository.Verify(a => a.Edit(It.IsAny<Student>()), Times.Exactly(1));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Delete_WhenCalled_ThrowAnInvalidStudentIdExceptionWhenStudentIdDoesnotExists()
        {
            student.student_id = 0;
            studentService.Delete(student);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Delete_WhenCalled_ThrowStudentDoesnotExistsExceptionWhenStudnetIsNull()
        {
            student.student_name = null;
            studentService.Delete(student);
        }

        [TestMethod]
        public void Delete_WhenCalled_TestThatDataWillBeDeletedFromDatabaseorNot()
        {
            IStudentRepository.Setup(a => a.GetSingleStudentById(It.IsAny<int>())).Returns(student);
            studentService.Delete(student);
            IStudentRepository.Verify(a => a.Delete(It.IsAny<Student>()), Times.Exactly(1));
        }
    }
}
