﻿using DotnetApp.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotnetAppTesting.Service_Testing
{
    [TestClass]
    public class MathServiceTest
    {
        private MathService _mathService;
        [TestInitialize]
        public void Setup()
        {
            _mathService = new MathService();
        }

        [TestMethod]
        public void Add_WhenCalled_ReturnSum()
        {
            var result = _mathService.Add(1, 2);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void Subtract_WhenCalled_ReturnDifference()
        {
            var result = _mathService.Subtract(2, 1);
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void Multiply_WhenCalled_ReturnProduct()
        {
            var result = _mathService.Multiply(2, 3);
            Assert.AreEqual(6, result);
        }

        [TestMethod]
        public void Max_WhenFirstNumberIsGreater_ReturnFirstNumber()
        {
            var result = _mathService.Max(2,1);
            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void Max_WhenSecondNumberIsGeater_ReturnSecondNumber()
        {
            var result = _mathService.Max(1,2);
            Assert.AreEqual(2,result);
        }

        [TestMethod]
        public void Max_WhenBothNumberAreEqual_ReturnSameNumber()
        {
            var result = _mathService.Max(1, 1);
            Assert.AreEqual(1, result);
        }

    }
}
