﻿using DotnetApp.Models;
using DotnetApp.Repository.Interface;
using DotnetApp.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotnetAppTesting.Service_Testing
{
    [TestClass]
    public class GradeServiceTest
    {
        Mock<IGradeRepository> IGradeRepository = new Mock<IGradeRepository>();

        Grade grade;
        GradeService gradeService;

        [TestInitialize]
        public void Setup()
        {
            gradeService = new GradeService(IGradeRepository.Object);
            grade = new Grade()
            {
                grade_id = 1,
                grade_name = "One",
            };
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Create_WhenCalled_ThrowGradeNameAlreadyExistsExceptionWhenGradeAlreadyExists()
        {
            IGradeRepository.Setup(a => a.GetSingleGradeByName(It.IsAny<string>())).Returns(new Grade());
            gradeService.Create(grade);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Update_WhenCalled_ThrowGradeAlreadyExistsExceptionWhenGradeAlreadyExists()
        {
            IGradeRepository.Setup(a => a.GetSingleGradeByName(It.IsAny<string>())).Returns(new Grade());
            gradeService.Update(grade);
        }

        [TestMethod]
        public void Create_WhenCalled_TestThatDataWillInsertIntoDatabaseWhenGradeNameIsNull()
        {
            IGradeRepository.Setup(a => a.GetSingleGradeByName(null));
            gradeService.Create(grade);
        }

        [TestMethod]
        public void Update_WhenCalled_TetsThatDataWillUpdateIntoDatatbaseWhenGradeNameIsNull()
        {
            IGradeRepository.Setup(a => a.GetSingleGradeByName(null));
            gradeService.Update(grade);
        }

        [TestMethod]
        public void Create_WhenCalled_TestThatDataWillSavedIntoDatabaseOrNot()
        {
            IGradeRepository.Setup(a => a.Create(It.IsAny<Grade>())).Verifiable();
            gradeService.Create(grade);
            IGradeRepository.Verify(a => a.Create(It.IsAny<Grade>()), Times.Exactly(1));
        }

        [TestMethod]
        public void Update_WhenCalled_TestThatDataWillUpdateInDatabaseOrNot()
        {
            IGradeRepository.Setup(a => a.Edit(It.IsAny<Grade>())).Verifiable();
            gradeService.Update(grade);
            IGradeRepository.Verify(a => a.Edit(It.IsAny<Grade>()), Times.Exactly(1));
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Delete_WhenCalled_ThrowInvalidGradeIdExceptionWhenGradeIdDoesnotExists()
        {
            grade.grade_id = 0;
            gradeService.Delete(grade);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Delete_WhenCalled_ThrowGradeDoesNotExistsExceptionWhernGradeIsNull()
        {
            grade.grade_name = null;
            gradeService.Delete(grade);
        }

        [TestMethod]
        public void Delete_WhenCalled_TestThatDataWillDeleteFromDatabaseOrNot()
        {
            IGradeRepository.Setup(a => a.GetSingleGradeById(It.IsAny<int>())).Returns(grade);
            gradeService.Delete(grade);
            IGradeRepository.Verify(a => a.Delete(It.IsAny<Grade>()),Times.Exactly(1));
        }
    }
}
